;;; xwl-calendar.el --- calendar related stuffs

;; Copyright (C) 2007, 2008, 2009 William Xu

;; Author: William Xu <william.xwl@gmail.com>

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, write to the Free Software
;; Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
;; 02110-1301 USA

;;; Code:

(require 'calendar)
(require 'cal-china-x)

(setq calendar-week-start-day 1)

(setq calendar-latitude +39.55
      calendar-longtitude +116.25
      calendar-location-name "Beijing")
;; changting(+25.52, +116.20)

(global-set-key (kbd "<f12>") (lambda () (interactive)
                                (let ((cal "*Calendar*"))
                                  (if (get-buffer cal)
                                      (progn
                                        (split-window-vertically -9)
                                        (other-window 1))
                                    ;; 禁用垂直分割。
                                    (let ((split-width-threshold 9999))
                                      (calendar)))
                                  (switch-to-buffer cal)
                                  (calendar-cursor-holidays))))

(setq appt-issue-message t
      mark-holidays-in-calendar t
      view-calendar-holidays-initially t)

(when (file-readable-p "~/.diary")
  (setq diary-file "~/.diary")
  (setq mark-diary-entries-in-calendar t))

(appt-activate 1)
(setq appt-message-warning-time 1)

(defun xwl-current-year ()
  (string-to-number (format-time-string "%Y" (current-time))))

(defun xwl-increment-date (date)
  (calendar-gregorian-from-absolute (1+ (calendar-absolute-from-gregorian date))))

(defun xwl-holiday-fixed-range (month start-day end-day string)
  "[start-day, end-day], range is within a month."
  (let* ((date (list month start-day (xwl-current-year)))
         (ret `((holiday-fixed ,(calendar-extract-month date)
                               ,(calendar-extract-day date)
                               ,string))))
    (while (not (= (calendar-extract-day date) end-day))
      (setq date (xwl-increment-date date))
      (setq ret (append ret `((holiday-fixed ,(calendar-extract-month date)
                                             ,(calendar-extract-day date)
                                             ,string)))))
    ret))

(setq xwl-company-holidays
      (append (xwl-holiday-fixed-range 1 1 3 "*company 元旦*")
              (xwl-holiday-fixed-range 1 25 31 "*company 春节*")
              (xwl-holiday-fixed-range 4 4 6 "*company 清明节*")
              (xwl-holiday-fixed-range 5 1 3 "*company 劳动节*")
              (xwl-holiday-fixed-range 5 28 30 "*company 端午节*")
              (xwl-holiday-fixed-range 10 1 8 "*company 国庆节*")))

(setq cal-china-x-priority1-holidays
      (append cal-china-x-priority1-holidays
              '((holiday-lunar 5 11 (xds ",f`I,,h<,,J],g\\c..p9"))
                (holiday-lunar 12 30 "過年啦！")
                (holiday-lunar 4 13  (xds "Xd0hYp;edfKecA&dc-Omm@<=")))
              xwl-company-holidays))

(setq rockets-schedule
      '(
        ;; 10.30  休斯敦火箭     孟菲斯灰熊     "08:30 上海体育   CCTV5 新传"
        02.01 勇士  火箭 "09:30am （CCTV5）"
        02.04 公牛  火箭 "09:30am （新传体育）"
        02.05 火箭  灰熊 "09:00am （CSPN/上海体育/北京体育/广东体育/劲爆体育）"
        02.08 狼队  火箭 "09:30am （CSPN/上海体育/北京体育/广东体育/劲爆体育）"
        02.10 火箭  雄鹿 "09:00am （CCTV5）"
        02.12 国王  火箭 "09:30am"
        02.18 篮网  火箭 "09:30am （CCTV5）"
        02.21 小牛  火箭 "09:30am （CCTV5）"
        02.23 山猫  火箭 "06:00am （CSPN/上海体育/北京体育/广东体育）"
        02.25 开拓  火箭 "09:30am （CCTV5）"
        02.27 骑士  火箭 "09:00am （新传体育/搜狐体育）"

        ))

(setq cal-china-x-priority2-holidays
      (let ((events '())
            (nba rockets-schedule)
            date event)
        (while nba
          (setq date (car nba))
          (setq event (format "%S %S %S" (nth 1 nba) (nth 2 nba) (nth 3 nba)))
          (setq nba (cdr (cdr (cdr (cdr nba)))))
          (setq events (cons `(holiday-fixed ,(floor date)
                                             ,(% (floor (* 100 date)) 100)
                                             ,event)
                             events)))
        events))

(setq other-holidays
      '((holiday-fixed 10 10 "民国双十节")
        (holiday-fixed 10 11 "靚穎生日快樂！")
	;; (holiday-fixed 3  8  "妇女节")
        (holiday-fixed 3  12 "植树节")
        (holiday-fixed 5  4  "青年节")
        ;; (holiday-fixed 6  1  "儿童节")
        (holiday-fixed 9  10 "教师节")
        (holiday-lunar 1 15 "元宵节")
        (holiday-lunar 7 7  "七夕节")
        (holiday-lunar 9 9  "重阳节")))

(setq calendar-holidays
      (append cal-china-x-priority1-holidays
              cal-china-x-priority2-holidays
              other-holidays))

(define-key calendar-mode-map (kbd "j") 'calendar-forward-week)
(define-key calendar-mode-map (kbd "k") 'calendar-backward-week)
(define-key calendar-mode-map (kbd "l") 'calendar-forward-day)
(define-key calendar-mode-map (kbd "h") 'calendar-backward-day)

;; (add-hook 'today-visible-calendar-hook 'calendar-star-date)

(when (> emacs-major-version 22)
  (defun xwl-display-diary ()
    (when (eq (face-at-point) 'diary)
      (save-window-excursion
        (message (nth 1 (car (diary-view-entries)))))))

  (add-hook 'calendar-move-hook 'xwl-display-diary)

  )

(defadvice insert-diary-entry (around disable-less activate)
  (let ((find-file-hook (remove 'less-minor-mode-on find-file-hook)))
    ad-do-it))

(provide 'xwl-calendar)

;; xwl-calendar.el ends here
