;;; xwl-comint.el --- comint modes

;; Copyright (C) 2007, 2008 William Xu

;; Author: William Xu <william.xwl@gmail.com>
;; Last updated: 2008/04/20

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with EMMS; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
;; Boston, MA 02110-1301, USA.

;;; Code:

;; comint, *shell*

(eval-after-load 'ansi-color
  '(progn
     (add-hook 'comint-mode-hook 'ansi-color-for-comint-mode-on)))

(add-hook 'comint-mode-hook 'turn-off-auto-fill)
(add-hook 'comint-output-filter-functions 'shell-strip-ctrl-m)
(add-hook 'comint-output-filter-functions 'comint-watch-for-password-prompt)

;; xterm's isearch alike
(setq xwl-comint-shell-search-keyword "")
(make-local-variable 'xwl-comint-shell-search-keyword)

(defun xwl-comint-shell-search-backward (&optional keyword)
  (interactive
   (list (read-from-minibuffer
          (if (and xwl-comint-shell-search-keyword
                   (not (string= xwl-comint-shell-search-keyword "")))
              (format "Search backward shell command (regexp = %s): "
                      xwl-comint-shell-search-keyword)
            "Search backward shell command (regexp): "))))
  (unless (string= keyword "")
    (setq xwl-comint-shell-search-keyword keyword))
  (comint-previous-matching-input xwl-comint-shell-search-keyword 1))

(defun xwl-comint-shell-search-forward (&optional keyword)
  (interactive
   (list (read-from-minibuffer
          (if (and xwl-comint-shell-search-keyword
                   (not (string= xwl-comint-shell-search-keyword "")))
              (format "Search forward shell command (regexp = %s): "
                      xwl-comint-shell-search-keyword)
            "Search forward shell command (regexp): "))))
  (unless (string= keyword "")
    (setq xwl-comint-shell-search-keyword keyword))
  (comint-next-matching-input xwl-comint-shell-search-keyword 1))

(eval-after-load 'comint
  '(progn
     (define-key comint-mode-map (kbd "M-p") (lambda ()
                                               (interactive)
                                               (comint-previous-input 1)))
     (define-key comint-mode-map (kbd "M-n") (lambda ()
                                               (interactive)
                                               (comint-next-input 1)))
     (define-key comint-mode-map (kbd "M-r") 'xwl-comint-shell-search-backward)
     (define-key comint-mode-map (kbd "M-s") 'xwl-comint-shell-search-forward)

     (define-key comint-mode-map (kbd "C-l")
       (lambda ()
         (interactive)
         (if current-prefix-arg
             (call-interactively 'recenter)
           (let ((inhibit-read-only t))
             (erase-buffer)
             (comint-send-input)
           ;; (recenter 0)
           ;; (xwl-scroll-up-one-line scroll-margin)
           ))))
     ))

(eval-after-load 'shell
  '(progn
     (define-key shell-mode-map (kbd "C-c m R") ;; 'rename-uniquely)
       'rename-buffer)
     (define-key shell-mode-map (kbd "C-d") 'delete-char)
     (define-key shell-mode-map (kbd "C-c C-c") 'xwl-disable-key)))

(provide 'xwl-comint)

;;; xwl-comint.el ends here
