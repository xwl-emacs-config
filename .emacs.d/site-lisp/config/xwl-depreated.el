;;; xwl-depreated.el --- deprecated stufffs

;; Copyright (C) 2007 William Xu

;; Author: William Xu <william.xwl@gmail.com>
;; Version: 0.1

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with EMMS; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
;; Boston, MA 02110-1301, USA.

;;; Commentary:

;;

;; Put this file into your load-path and the following into your
;; ~/.emacs:
;;           (autoload 'xwl-depreated "xwl-depreated")

;;; Code:

;; elpa
;; ----

;; (let ((buffer (url-retrieve-synchronously
;; 	       "http://tromey.com/elpa/package-install.el")))
;;   (save-excursion
;;     (set-buffer buffer)
;;     (goto-char (point-min))
;;     (re-search-forward "^$" nil 'move)
;;     (eval-region (point) (point-max))
;;     (kill-buffer (current-buffer))))

;; (require 'package)
;; (package-initialize)

;; Use posting-style is the better way

;; (defun xwl-sendmail-select ()
;;   "Select sendmail methods. You know, some ML doesn't allow
;; sendmail directly from localhost without a valid domain name."
;;   (save-excursion
;;     (let ((to (save-excursion
;;                 (message-narrow-to-headers)
;;                 (or (message-fetch-field "to")
;;                     "")))
;;           (gcc (save-excursion
;;                  (message-narrow-to-headers)
;;                  (or (message-fetch-field "Gcc")
;;                      ""))))
;;       (cond ((string-match "lists.sourceforge.net" to)
;;              (message "Will sendmail by gmail")
;;              (xwl-sendmail-by-gmail))
;;             ((or (string-match ".*abc.net.*" to)
;;                  (catch 'return
;;                    (progn (mapc
;;                            (lambda (el)
;;                              (if (string-match el gcc)
;;                                  (throw 'return t)))
;;                            xwl-ce-groups)
;;                           nil)))
;;              (xwl-sendmail-by-ce))
;;             (t
;;              ;;(xwl-sendmail-by-localhost)
;;              (xwl-sendmail-by-gmail)))
;;       )))

;; (remove-hook 'message-setup-hook 'xwl-sendmail-select)

;; send-hook

;; Fix "From" for trac changelogs.
;; (add-hook 'gnus-article-prepare-hook 'xwl-fix-trac-log-author)

(defun xwl-fix-trac-log-author ()
  (cond ((string-match "\\`nnrss:.*Revisions.*" gnus-newsgroup-name)
         (save-excursion
           (goto-char (point-min))
           (unless (re-search-forward "^From:" nil t 1)
             (let ((author "nobody"))
               (re-search-forward "^Author:")
               (forward-line 1)
               (skip-chars-forward "[[:space:]]")
               (setq author (buffer-substring-no-properties
                             (point)
                             (progn (end-of-line)
                                    (skip-chars-backward "[[:space:]]")
                                    (point))))
               (gnus-summary-edit-article 2)
               (goto-char (point-min))
               (insert (format "From: %s <%s@abc.net>\n" author author))
               (let ((gnus-article-prepare-hook nil))
                 (gnus-article-edit-done))))))))

;;; xwl-depreated.el ends here
