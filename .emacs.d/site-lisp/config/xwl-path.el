(setq xwl-load-path "~/.emacs.d/site-lisp")

(setq load-path
      (append (list xwl-load-path
                    "~/repo/cvs/mmm-mode"
                    "~/repo/misc/auctex-11.84"
                    "~/repo/cvs/bbdb/lisp"
                    "~/repo/git/emms/lisp"

                    "~/repo/git/elisp"
                    "~/repo/git/elisp/dashboard"
                    "~/repo/git/elisp/generic-apt"
                    "~/repo/git/dvc/lisp"

                    ;; Weird w3m! Without this, `emacs -batch' will complain.
                    ;; "~/share/emacs/site-lisp/w3m"

                    "~/repo/cvs/emacs-w3m"
                    "~/repo/svn/chicken-eggs"

                    "~/etc"
                    )
              load-path))


(when (< emacs-major-version 23)
  (setq load-path (append '("~/repo/git/erc"
                            "~/repo/git/remember"
                            "~/repo/git/org-mode/lisp"
                            "~/repo/cvs/tramp/lisp")
                          load-path)))

(setq Info-directory-list
      (append '("~/info/"
                "~/share/info"
                "/sw/share/info"
                "~/repo/git/dvc/texinfo"

                "/sw/lib/chicken/1/"
                )
	      Info-default-directory-list))

(let ((orig default-directory))
  (cd xwl-load-path)
  (normal-top-level-add-subdirs-to-load-path)
  (cd orig))

;;; xwl-path.el ends here
