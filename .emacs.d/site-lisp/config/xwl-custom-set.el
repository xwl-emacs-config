;;; xwl-custom-set.el --- custom sets

;; Copyright (C) 2009 William Xu

;; Author: William Xu <william.xwl@gmail.com>
;; Version: 0.1

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston,
;; MA 02110-1301, USA.

;;; Commentary:

;; Do not load it at last in .emacs, or it will mess with my color theme setup!!

;;; Code:

(custom-set-variables
  ;; custom-set-variables was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
 '(LaTeX-section-hook (quote (LaTeX-section-heading LaTeX-section-section)))
 '(Man-width 78)
 '(bbdb-update-records-mode (quote (quote searching)))
 '(erc-track-showcount t)
 '(ido-auto-merge-delay-time 1000)
 '(message-syntax-checks (quote ((sender . disabled) (signature . disabled))))
 '(safe-local-variable-values (quote ((longlines-mode . t))))
 '(thinks-from (quote bottom-diagonal)))
(custom-set-faces
  ;; custom-set-faces was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
 '(diary ((((min-colors 88) (class color) (background light)) (:background "yellow"))))
 '(emms-playlist-selected-face ((t (:inherit (gnus-summary-selected gnus-summary-normal-read)))))
 '(emms-playlist-track-face ((t nil)))
 '(erc-keyword-face ((t (:foreground "green" :weight bold))))
 '(font-lock-constant-face ((((class color) (min-colors 88) (background light)) (:foreground "magenta"))))
 '(font-lock-string-face ((((class color) (min-colors 88) (background light)) (:foreground "green4"))))
 '(gnus-signature ((t (:slant italic))))
 '(gnus-summary-high-read ((t (:foreground "magenta"))))
 '(gnus-summary-high-unread ((t (:foreground "magenta"))))
 '(holiday ((((class color) (background light)) (:background "RoyalBlue"))))
 '(mmm-default-submode-face ((t (:background "gray18"))))
 '(org-done ((t (:foreground "ForestGreen" :strike-through "red"))))
 '(org-scheduled-today ((((class color) (min-colors 88) (background light)) (:foreground "ForestGreen")))))

(provide 'xwl-custom-set)

;;; xwl-custom-set.el ends here
