;;; xwl-private.el

;; These will go to `mail.genenal'.
(defvar xwl-mailbox-lists '())

(defvar pw163 "")
(defvar pwsohu "")
(defvar pwsina "")
(defvar pwyahoo "")
(defvar pwhotmail "")
(defvar pwbbs "")
(defvar pwtsh "")
(defvar pwsun "")
(defvar pwsql "")
(defvar pwgmail "")
(defvar pwerc "")
(defvar pwdeb "")
(defvar pwce "")
(defvar pwlastfm "")
(defvar pwbitlbee "")

(ignore-errors (require 'xwl-private-setup))

(provide 'xwl-private)

;;; xwl-private.el ends here
