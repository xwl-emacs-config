;;; xwl-org.el --- configs for org-mode

;; Copyright (C) 2008 William Xu

;; Author: William Xu <william.xwl@gmail.com>
;; Version: 0.1

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston,
;; MA 02110-1301, USA.

;;; Code:

;; org
(require 'org-install)

(add-to-list 'auto-mode-alist '("\\.org$" . org-mode))

(setq org-todo-keywords
      '(
        (sequence "-" ">" "o")
        (sequence "TODO" "DONE")
        (sequence "STARTED" "WAITING" "LATER" "CANCELLED")
        (sequence "x")
        ))

(add-hook 'org-mode-hook
          (lambda ()
            (modify-syntax-entry ?- "w" org-mode-syntax-table)
            (modify-syntax-entry ?> "w" org-mode-syntax-table)))


(setq xwl-org-tag-alist '("@work" "@hacking" "@emacs" "@life" "@misc"
                          "@jojo" "@language" "@blog" "@mail" "@reading"))

(defun xwl-org-set-tags (tag)
  "Insert TAG under point."
  (interactive
   (list (ido-completing-read "Set tag: " xwl-org-tag-alist)))
  (let ((inhibit-read-only t))
    (save-excursion
      (move-end-of-line 1)
      (insert (format "  :%s:" tag)))))

(eval-after-load 'org
  '(progn
     (define-key org-mode-map (kbd "C-c C-g") 'xwl-org-set-tags)
     ))

(eval-after-load 'org-agenda
  '(progn
     (setq org-agenda-files '("~/notes/todo.org"))

     (define-key org-agenda-mode-map (kbd "N") (lambda ()
                                                 (interactive)
                                                 (forward-char 1)
                                                 (re-search-forward "^[a-zA-Z]" nil t 1)
                                                 (backward-char 1)))

     (define-key org-agenda-mode-map (kbd "P") (lambda ()
                                                 (interactive)
                                                 (re-search-backward "^[a-zA-Z]" nil t 1)))

     (defadvice org-agenda-list (after maximize-window activate)
       (delete-other-windows))
     ))

;; remember
(require 'remember)
(add-hook 'remember-mode-hook 'org-remember-apply-template)

(setq remember-handler-functions '(org-remember-handler))

(global-set-key (kbd "C-c r") 'remember)

(setq org-remember-templates
      '((?t "* - %?\n  %u" "~/notes/todo.org")
        ;; (?n "* %u %?" "~/.notes")
        ))

(provide 'xwl-org)

;;; xwl-org.el ends here
